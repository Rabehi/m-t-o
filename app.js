const express = require('express')
const app = express()

app.get('/', function (req, res) {
  var ip = require('ip');
  res.send('Hello fom host ' + ip.address())
})

app.get('/skycolor', function (req, res) {
  res.send('rouge')
})

app.get('/healthz', function (req, res) {
  res.send('Health Check : OK')
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


